# Maxscript CSharp Invoker

Simple maxscript functions that will help using C# methods containing optional arguments.

```Csharp
public class MyClass {
    public int TheFunction(int projectId, string sheetName, string columnName,
                            int color=121212, bool isEditable=true, 
                            bool isLocked=true, int order=0,
                            string description="")
}
```

```lua
invoker myClassInstance "TheFunction" #(42, "sheetName", "columnName", #(#description, "my description"))
```

It's also possible to use the named parameter syntax for positional arguments.

```lua
-- Usage : invoker obj fnName arrayOfArguments
--         named parameter are arrays of #(#nameValue, object)
invoker myClassInstance "TheFunction" #( #(#sheetName, "sheetName", #(#projectId, 42), #(#columnName,"columnName") )
```

Exceptions will be thrown if positional arguments count is invalid or if positional arguments are placed after named arguments.

This will obviously fail if TheFunction requires as argument an array of #(#namedValue, object).
