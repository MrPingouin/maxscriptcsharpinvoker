-- Maxscript CSharp Invoker
-- Laurent 'MrPingouin' CHEA for TAT Studio
-- 2023/01/25

-- Usage : invoker obj fnName arrayOfArguments
--         named parameter are arrays of #(#nameValue, object)
-- NOTE : will obviously fail on array matching pattern #(nameValue, object)

function isNamedParameter arg = (
	return classof arg == Array and arg.count == 2 and classof arg[1] == Name
)

function checkArgumentsArray params callArgs = (
	-- Checking that named parameters, if any, are correctly placed at the END of the callArgs array
	-- Checking that named parameters exist as well
	local namedArgFound = false
	local positionalArgumentCount = 0
	for i=1 to callArgs.count do (
		local arg = callArgs[i]
		if isNamedParameter arg then (
			namedArgFound = true
			
			local namedArgExists = (for p in params where (p.name as name == arg[1]) collect p).count > 0
			if not namedArgExists then (
				throw ("Argument '" + arg[1] + "' doesn't exist")
			)
			
			local namedArgIsPositional = (for p in params where (p.name as name == arg[1]) and not p.isOptional collect p).count > 0
			if namedArgIsPositional then (
				positionalArgumentCount += 1
			)
		)
		else (
			positionalArgumentCount += 1
			if namedArgFound then (
				throw ("Positional argument " + (i as string) + " (" + (arg as string) + ") cannot be placed AFTER named parameters")
			)
		)
	)
	
	-- checking positional arguments count
	local requiredPositionalArgumentCount = (for p in params where not p.isOptional collect p).count
	
	if positionalArgumentCount != requiredPositionalArgumentCount then (
		throw ("Position arguments count invalid : found " + (positionalArgumentCount as string) + ", expecting " + (requiredPositionalArgumentCount as string))
	)
)

function invoker obj fnName callArgs = (
	local f = (obj.getType()).getMethod(fnName)
	local params = f.getParameters() -- WARNING : 1-based array
	checkArgumentsArray params callArgs
		
	-- Creating an object array of size "function parameters count".
	local trueArgs = dotNetObject "object[]" params.count
		
	-- Initializing all default arguments
	for i=1 to params.count where params[i].HasDefaultValue do (
		trueArgs[i-1] = params[i].defaultValue
	)
		
	-- Filling positional arguments
	for i=1 to params.count where not params[i].isOptional and not (isNamedParameter callArgs[i]) do (
		format "param [%] % : %\n" i params[i].name (callArgs[i] as string)
		trueArgs[i-1] = callArgs[i]
	)
	
	-- Filling named arguments
	for arg in callArgs where isNamedParameter arg do (
		local argName = arg[1]
		local argValue = arg[2]
		
		for i=1 to params.count do (
			if (params[i].name as name) != argName then (
				continue
			)
			format "param [%] % : %\n" i params[i].name (argValue as string)
			trueArgs[i-1] = argValue
		)
	)
	
	f.invoke obj trueArgs
)


-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@&&&&&&&&&%%%%%%%%%%%%%%%##%###%%%%%%%%&&&&&&@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
-- @@@@@@@@@@@@@@@@@@@@@@@@@@@&&&&&&%%%%%%########(((///***,,,,,,,/(####%%%%&&&&@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
-- @@@@@@@@@@@@@@@@@@@@@@@@&&&&&&%%%%%#####((((/**,.......      ...  .    ..*(&&&&@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
-- @@@@@@@@@@@@@@@@@@@@@@&&&&&%%%%%%####(((/**,....        .,...         ... ...&&&&&@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
-- @@@@@@@@@@@@@@@@@@@@&&&&&&%%%%%%###((/**,.....     .,/.........       .......,*&&&&@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
-- @@@@@@@@@@@@@@@@@@&&&&&&&&&%%%%%##(/**,..........,(*,,,.............  .......,**(@&&&@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
-- @@@@@@@@@@@@@@&&&&&&&&&&&&&&%%%%#/*,,.....,....,(*,**,,,,....................,***%@&@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
-- @@@@@@@&&&&&&&&&&&&&&&&&&&&&&%%%/*,,.,,,,,..,,//,*****/((///*,...............,,*//@&&&@@@@@&@@@@@@@@@@@@@@@@@@@@@@@@@@@@
-- @@@&&&&&&&&&&&&&&&&&&&&&&&&&&%%(*,,,,,*,,,,,,/,..*/((#%%####%%%(/*,,,,.....,,,,*((&&&&&&&&&&&&&&&&&&&&&&&@&&&@@@@@@@@@@@
-- &&&&&&&&&&&&&&&&&&&&&&&&&&&%##(/*,*/*/,*,,***,,...,/%%%#/...,*(#%%%/****,,,*****((%%%%%%%%%%&&&&&&&&&&&&&&&&&&&&@@@@@@@@
-- &&&&&&&&&&&&&&&&&%%%%%#######(/(((/*/********,,.......,((//*. .(#%%%%(/***//////#(%%%%%%%%%%%%%%%&&&&&&&&&&&&&&&&&&&&&&&
-- &&&&%%%%%%##################/*,*/(((/*/,**//**,,,.....,,*///(#(/,,**,,..#%%%#####(%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
-- ###########################(/*///((///***/##(((/**,,,,,,,***,,,,,*****,,/   /(###(%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
-- ###########################%///**/(//////#&%####((/*********,,,**////,,,##(/*/(#((%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%&&&&&%%
-- #######################(#%#((#///((((/(/(%&%%####(((////******/((((//*,,//***/####%%%%%%%%%%%%%%%&&&&&&&&&&&&&&&&&&&&&&&
-- ####################(/**##(((#%%##((((//#&&%%###((((((//******(%&&&%####/,,**/(#(%%%%%%%&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
-- %%%%%%%########///#%%%%%%#((#%&&%#((((/(#&%#(###((((/////****///**#%%%%#****//(#(/%%&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
-- %%%%%%%%%%%%%(//#%%%%%%%%###%%&&%###(((#%&##((((#(((/////*****,*//****(#(//(##%%%%((#(&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
-- &&&&&&&&&%%%%#//#&%%&%%%%###%%%%#####((#%%##((((###((((%%%%%%%%&&%%%%%#((#%%%%&%&&&###(&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&%(#
-- &&&&&&&&&&&&%%*,,*&%%&%%%##%%%%%#######%&%####(########(/////((##((((%%##%%&&&%&@@#####&&&&&&&&&&&&&&&&&&&&&&&&&&&&%(((#
-- &&@@@@@@@&&&&&&/,,,/%&&%%%%%%%%##%#%###%&%%############((((##%%%%%%%#(%%%%&&&%%@%####%&&&&&&&&&&&&&&&&&&&&&&&&&&#////%@@
-- &@@@@@@@@@&&&&&&&/,..*#%%%%%%%%#%%%%###&&&%%%##########((((((/*/(%%%%%%%%&&&%%%((#%&%%@@&&&&&&&&&&&&&@@@@&&&&(///*#@&@@@
-- &@@@@@@@@@@@&&&%%&&#,..,,/%%%%%%%%%%%#%&&&&&&&%%######(////******/(%%##%&&&#*,*/%&&%%@@@@@@@@@@@@@@@@@@@@%/////%@@@@@@@@
-- &&@@@@@@@@@@&&&%%%%&&&#,,..**#%%%%%%%%%&&&&&&&&&&&&%%%%#(((///((/((####&%#*,,#%&&%##&@@@@@@@@@@@@@@@@&(////#&&@@@@@@@@@@
-- @@@@@@@@@@@@@&&%%%%%&&&&&%&&&&%&&%%%%%%&@@&&&&&&&&&&&&&&&&&%%%&%%%%%%%@&%&&(&&&%###%@@@@@@@@@@@@@&/****(&@@@@@@@@@@@@@@@
-- %&&@@@@@@@@@@&@&%%%%%%&&&&&&&&&%&%%%%%%&@@@@@&&&&&&&&&&&&&&&&&&&&&&&&@&&&&&%&%####%%@@@@@@@@@(,,**,/&@&@@&@@@@@@@@@@@@@@
-- //#@@@@@@@@@@&&&&%%%%%%%&&&&&&&&&&%%%%%&@@@@@@@@@&&&&&&&&&&&&&@@@@@@@&&&&&&&%####%%%@@@@#,.,,,/%&&&@&&&&@@@@@@@@@@@@@@@@
--
-- Thus I invoke myself!
